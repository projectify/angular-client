var myApp = angular.module('myApp', [
  'ngRoute',
  'userController'
]);

myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/user/signup', {
        templateUrl: 'partials/user/signup.html',
        controller: 'UserSignupController'
    }).
    otherwise({
        redirectTo: '/user/signup'
    });
}]);